<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Faker\Factory;
use App\Models\Plano;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ClienteTest extends TestCase
{
    use RefreshDatabase,WithoutMiddleware;

    public function setUp(): void
    {
        parent::setUp();
        \Artisan::call('db:seed');
        $this->faker = Factory::create();
    }
    
    public function test_example()
    {        
        $data = [
            'nome'      => $this->faker->name(),
            'email'     => $this->faker->unique()->safeEmail(),
            'telefone'  => $this->faker->phoneNumber(),
            'cidade'    => $this->faker->city(),
            'estado'    => $this->faker->stateAbbr(),
            'dtnasc'    => $this->faker->date(),
            'planos'    => $this->amarra_clientes_planos()
        ];
        
        $teste = $this->post('/api/clientes/store', $data)
        ->assertStatus(200);

        $teste->dumpSession();

        
    }
    
    public function amarra_clientes_planos()
    {
        
        $planos = Plano::select('id_plano as plano_id')->get()->toArray();
        
        $random = $this->array_random($planos,rand(1, 3));
        
        return $random;
    }
    
    public static function array_random($array, $amount = 1)
    {
        $keys = array_rand($array, $amount);
        
        if ($amount == 1) {
            return $array[$keys];
        }
        
        $results = [];
        foreach ($keys as $key) {
            $results[] = $array[$key];
        }
        
        return $results;
    }
}