<?php

use Illuminate\Database\Seeder;

use App\Models\Cliente;
use App\Models\Plano;


class DatabaseSeeder extends Seeder
{
    /**
    * Seed the application's database.
    *
    * @return void
    */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('planos')->truncate();
        DB::table('clientes')->truncate();
        DB::table('cliente_planos')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->clientes();
        $this->planos();
        $this->amarra_clientes_planos();
    }

    public function clientes()
    {
        $path = app();
        $base = $path->basePath();

        $arquivo = $base . "/database/seeders/clientes.json";

        $data = json_decode(file_get_contents($arquivo), true);

        foreach ($data['clientes'] as $key => $value) {

            //dd($value);
            $cliente            = new Cliente;
            $cliente->nome      = $value['nome'];
            $cliente->email     = $value['email'];
            $cliente->telefone  = $value['telefone'];
            $cliente->cidade    = $value['cidade'];
            $cliente->estado    = $value['estado'];
            $cliente->dtnasc    = date('Y-m-d',strtotime($value['dtnasc']));
            $cliente->save();

        }
    }

    public function planos()
    {
        $path = app();
        $base = $path->basePath();

        $arquivo = $base . "/database/seeders/planos.json";

        $data = json_decode(file_get_contents($arquivo), true);

        foreach ($data['planos'] as $key => $value) {
            $plano = new Plano;
            $plano->nome                = $value['nome'];
            $plano->mensalidade         = $value['mensalidade'];
            $plano->save();
        }
    }

    public function amarra_clientes_planos()
    {
        $clientes = Cliente::get();


        $planos = Plano::select('id_plano as plano_id')->get()->toArray();

        foreach($clientes as $cliente){
            $random = $this->array_random($planos,rand(1, 3));
            $cliente->planos()->sync($random);
        }
    }

    public static function array_random($array, $amount = 1)
    {
        $keys = array_rand($array, $amount);

        if ($amount == 1) {
            return $array[$keys];
        }

        $results = [];
        foreach ($keys as $key) {
            $results[] = $array[$key];
        }

        return $results;
    }
}
