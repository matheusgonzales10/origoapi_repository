<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PlanoRepository;
use App\Models\Plano;
use App\Validators\PlanoValidator;

/**
 * Class PlanoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PlanoRepositoryEloquent extends BaseRepository implements PlanoRepository
{
    public function model()
    {
        return Plano::class;
    }

    public function validator()
    {

        return PlanoValidator::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
