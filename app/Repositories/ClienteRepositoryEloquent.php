<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ClienteRepository;
use App\Models\Cliente;
use App\Validators\ClienteValidator;


class ClienteRepositoryEloquent extends BaseRepository implements ClienteRepository
{
    public function model()
    {
        return Cliente::class;
    }

    public function validator()
    {

        return ClienteValidator::class;
    }

    public function presenter()
    {
        //return "App\\Presenters\\ClientePresenter";
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
