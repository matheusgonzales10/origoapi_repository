<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Cliente;

/**
 * Class ClienteTransformer.
 *
 * @package namespace App\Transformers;
 */
class ClienteTransformer extends TransformerAbstract
{
    /**
     * Transform the Cliente entity.
     *
     * @param \App\Models\Cliente $model
     *
     * @return array
     */
    public function transform(Cliente $model)
    {
        return [
            'id'         => (int) $model->id,
            'estado'     => mb_strtoupper(trim($model->estado)),
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
