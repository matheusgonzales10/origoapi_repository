<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ClienteCreateRequest;
use App\Http\Requests\ClienteUpdateRequest;
use App\Repositories\ClienteRepository;
use App\Services\ClienteService;


class ClientesController extends Controller
{

    protected $repository;
    protected $service;

    public function __construct(ClienteRepository $repository, ClienteService $service)
    {
        $this->repository   = $repository;
        $this->service      = $service;
    }

    public function index(request $request)
    {
      $data = $this->repository->with('planos')->paginate(5);

      return response()->json($data,200);
    }

    public function store(ClienteCreateRequest $data)
    {
        $data = $this->service->store($data->all());

        if($data['success']){
            return response()->json($data,200);
        }else{
            return response()->json($data, 400);
        }
    }

    public function edit($id)
    {
        $data = $this->service->edit($id);

        if($data['success']){
            return response()->json($data,200);
        }else{
            return response()->json($data, 400);
        }
    }

    public function update(ClienteUpdateRequest $data,$id)
    {
        $data = $this->service->update($data->all(), $id);

        if($data['success']){
            return response()->json($data,200);
        }else{
            return response()->json($data, 400);
        }
    }

    public function destroy($id)
    {
        $data = $this->service->destroy($id);

        if($data['success']){
            return response()->json($data,200);
        }else{
            return response()->json($data, 400);
        }
    }
}
