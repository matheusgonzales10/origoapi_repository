<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PlanoCreateRequest;
use App\Http\Requests\PlanoUpdateRequest;
use App\Repositories\PlanoRepository;
use App\Services\PlanoService;


class PlanosController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct(PlanoRepository $repository, PlanoService $service)
    {
        $this->repository   = $repository;
        $this->service      = $service;
    }

    public function index(request $request)
    {
      $planos = $this->repository->paginate(5);

      return response()->json($planos,200);
    }

    public function store(PlanoCreateRequest $plano)
    {
        $plano = $this->service->store($plano->all());

        if($plano['success']){
            return response()->json($plano,200);
        }else{
            return response()->json($plano, 400);
        }
    }

    public function edit($id)
    {
        $plano = $this->service->edit($id);

        if($plano['success']){
            return response()->json($plano,200);
        }else{
            return response()->json($plano, 400);
        }
    }

    public function update(PlanoUpdateRequest $plano,$id)
    {
        $plano = $this->service->update($plano->all(), $id);

        if($plano['success']){
            return response()->json($plano,200);
        }else{
            return response()->json($plano, 400);
        }
    }

    public function destroy($id)
    {
        $plano = $this->service->destroy($id);

        if($plano['success']){
            return response()->json($plano,200);
        }else{
            return response()->json($plano, 400);
        }
    }
}
