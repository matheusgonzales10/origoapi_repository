<?php

namespace App\Services;

use App\Repositories\PlanoRepository;
use App\Validators\PlanoValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class PlanoService{
    
    private $repository;
    private $validator;
    
    public function __construct(PlanoRepository $repository, PlanoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }
    
    public function store($data)
    {
        try {

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
            $plano = $this->repository->create($data);

            return [
                'success' => true,
                'msg'     => 'Plano cadastrado com sucesso',
                'data'   => $plano
            ];
        } catch (\Exception $e) {
            return $this->error($e);
        }
    }

    public function edit($id)
    {
        try {

            $plano = $this->repository->find($id);

            return [
                'success' => true,
                'msg'     => 'Plano',
                'data'   => $plano
            ];
        } catch (\Exception $e) {
            return $this->error($e);
        }
    }
    
    public function update($data,$id)
    {
        try {

            $this->validator->with($data)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $plano = $this->repository->update($data,$id);

            return [
                'success' => true,
                'msg'     => 'Plano atualizado com sucesso',
                'data'   => $plano
            ];
        } catch (\Exception $e) {
            return $this->error($e);
        }
    }
    
    public function destroy($id)
    {
        try {
            
            $plano = $this->repository->find($id);
            $plano->delete();

            return [
                'success' => true,
                'msg'     => 'Plano deletado com sucesso',
            ];
        } catch (\Exception $e) {
            return $this->error($e);
        }
    }

    public function error($error)
    {
        if(method_exists($error,'getMessageBag')){
            return [
                'success' => false,
                'msg'     => 'Erro de execução',
                'error'   => $error->getMessageBag()
            ];
        }else{
            return [
                'success' => false,
                'msg'     => 'Erro de execução',
                'error'   => $error
            ];
        }
    }
}
