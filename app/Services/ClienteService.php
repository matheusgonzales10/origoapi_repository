<?php

namespace App\Services;

use App\Repositories\ClienteRepository;
use App\Validators\ClienteValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class ClienteService{
    
    private $repository;
    private $validator;
    
    public function __construct(ClienteRepository $repository, ClienteValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }
    
    public function store($data)
    {
        try {
            
            $cliente = $this->repository->create($data);
            
            $this->syncPlanos($cliente,$data['planos']);
            
            return [
                'success' => true,
                'msg'     => 'Cliente cadastrado com sucesso',
                'data'   => $cliente
            ];
        } catch (\Exception $e) {
            return $this->error($e);
        }
    }
    
    public function edit($id)
    {
        try {
            
            $cliente = $this->repository->with('planos')->find($id);
            
            return [
                'success' => true,
                'msg'     => 'Cliente',
                'data'   => $cliente
            ];
        } catch (\Exception $e) { 
            return $this->error($e);
        }
    }
    
    public function syncPlanos($cliente,$planos)
    {
        try {
            
            foreach ($planos as $key => $value) {
                $planos[] = (string)$value['id_plano'];
                unset($planos[$key]);
            }
            
            $cliente = $cliente->planos()->sync($planos);
            
            return $cliente;
            
        } catch (\Exception $e) { 
            return $this->error($e);
        }
        
    }
    
    public function update($data,$id)
    {
        try {
            
            $cliente = $this->repository->update($data,$id);
            
            $this->syncPlanos($cliente, $data['planos']);
            
            return [
                'success' => true,
                'msg'     => 'Cliente atualizado com sucesso',
                'data'   => $cliente
            ];
        } catch (\Exception $e) {
            return $this->error($e);
        }
    }
    
    
    public function destroy($id)
    {
        try {
            
            $cliente = $this->repository->find($id);
            $this->verifica_estado_regra($cliente);
            $cliente->delete();
            
            return [
                'success' => true,
                'msg'     => 'Cliente deletado com sucesso',
            ];
        } catch (\Exception $e) {
            
            return $this->error($e);
        }
    }
    
    public function verifica_estado_regra($cliente)
    {
        $exception = ['SP','SÃO PAULO','SAO PAULO', 'SAOPAULO'];
        
        if(in_array(trim(mb_strtoupper($cliente->estado)), $exception)) {
            throw new \Exception('Não é posssivel deletar clientes de SP');
        }
        
    }
    
    public function error($error,$message = null)
    {
        //dd($error);
        if(method_exists($error,'getMessageBag')){
            return [
                'success' => false,
                'msg'     => $message != null ? $message:'Erro de execução',
                'error'   => $error->getMessageBag()
            ];
        }else{
            return [
                'success' => false,
                'msg'     => $message != null ? $message:'Erro de execução',
                'error'   => $error->getMessage()
            ];
        }
    }
}
