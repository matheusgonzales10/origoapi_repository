<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PlanoValidator.
 *
 * @package namespace App\Validators;
 */
class PlanoValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'nome'          => ['required','unique:planos,nome,,id_plano'],
            'mensalidade'   => ['required']
        ],
        ValidatorInterface::RULE_UPDATE => [
            'nome'          => ['required','unique:planos,nome,,id_plano'],
            'mensalidade'   => ['required']
        ],
    ];

    protected $messages = [
        'nome.required'         => 'Informe o nome do plano',
        'nome.unique'           => 'Já existe esse plano no banco de dados',
        'mensalidade.required'  => 'Informe o valor do plano'
    ];
}
