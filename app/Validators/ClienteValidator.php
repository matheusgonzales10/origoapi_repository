<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class ClienteValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email'             => ['required','unique:clientes,email,,id_cliente'],
            'nome'              => ['required'],
            'telefone'          => ['required'],
            'cidade'            => ['required'],
            'estado'            => ['required'],
            'dtnasc'            => ['required','date'],
            'planos'            => ['required'],
            
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email'             => ['required','unique:clientes,email,,id_cliente'],
            'nome'              => ['required'],
            'telefone'          => ['required'],
            'cidade'            => ['required'],
            'estado'            => ['required'],
            'dtnasc'            => ['required','date'],
            'planos'            => ['required'],
        ],
    ];
    
    protected $messages = [
        'email.required'         => 'Informe o email do cliente',
        'email.email'            => 'Informe o email válido do cliente',
        'email.unique'           => 'Já existe esse email no banco de dados',
        'nome.required'          => 'Informe o nome do cliente',
        'email.required'         => 'Informe o email do cliente',
        'telefone.required'      => 'Informe o telefone do cliente',
        'cidade.required'        => 'Informe o cidade do cliente',
        'estado.required'        => 'Informe o estado do cliente',
        'dtnasc.required'        => 'Informe o data de nascimento do cliente',
        'dtnasc.date'            => 'Informe o data de nascimento do cliente',
        'planos.required'        => 'Informe pelo menos um plano para o cliente'
    ];
}
