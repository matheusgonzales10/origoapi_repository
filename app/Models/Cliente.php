<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
* Class Cliente.
*
* @package namespace App\Models;
*/
class Cliente extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $primaryKey = "id_cliente";
    
    protected $fillable = [
        'nome',
        'email',
        'telefone',
        'cidade',
        'estado',
        'dtnasc',
    ];
    
    
    
    public function planos()
    {
        return $this->belongsToMany(
            'App\Models\Plano',
            'cliente_planos',
            'cliente_id',
            'plano_id'
            )->select('id_plano')->without('');
        }
        
        
    }
    