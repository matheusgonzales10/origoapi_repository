<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Plano.
 *
 * @package namespace App\Models;
 */
class Plano extends Model implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = "id_plano";

    protected $fillable =['nome','mensalidade'];

    public function planos()
    {
        return $this->belongsToMany(
            'App\Models\Cliente',
            'cliente_planos',
            'plano_id',
            'cliente_id'
        );
    }
}
