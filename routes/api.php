<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'planos'
], function ($router) {
    Route::get('index', 'PlanosController@index');
    Route::post('store', 'PlanosController@store');
    Route::get('edit/{id}', 'PlanosController@edit');
    Route::put('update/{id}', 'PlanosController@update');
    Route::delete('destroy/{id}', 'PlanosController@destroy');
});

Route::group([
    'prefix' => 'clientes'
], function ($router) {
    Route::get('index', 'ClientesController@index');
    Route::post('store', 'ClientesController@store');
    Route::get('edit/{id}', 'ClientesController@edit');
    Route::put('update/{id}', 'ClientesController@update');
    Route::delete('destroy/{id}', 'ClientesController@destroy');
});
